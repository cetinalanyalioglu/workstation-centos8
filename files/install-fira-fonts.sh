#!/usr/bin/env bash
# Tested with CentOS 8

fonts_dir="/usr/share/fonts/fira-code"
if [-d ${fonts_dir}]
then
    echo "Already installed, returning"
    exit 0
fi

mkdir -p "${fonts_dir}"

for type in Bold Light Medium Regular Retina; do
    file_path="${fonts_dir}/FiraCode-${type}.ttf"
    file_url="https://github.com/tonsky/FiraCode/blob/master/distr/ttf/FiraCode-${type}.ttf?raw=true"
    if [ ! -e "${file_path}" ]; then
        echo "wget -O $file_path $file_url"
        wget -O "${file_path}" "${file_url}"
    else
 echo "Found existing file $file_path"
    fi;
done

echo "fc-cache -f"
fc-cache -f
