# An attempt to automate personal CentOS 8 workstation config
Go through the following steps to configure a CentOS 8 workstation.
## Structure
The way this setup is structured probably does not conform with ansible best practice suggestions. Enabling or disabling a feature is controlled via tags, no roles are defined.
## Steps to follow
* Start with a minimal CentOS 8 installation.
* Install ansible and dependencies
```
dnf install epel-release
dnf check-update
dnf install ansible
dnf install git
```
* Either run ansible-pull, which will make use of `local.yml`:
```
ansible-pull -U <repo-url> --checkout '<branch>' --tags <tags>
```

* Or clone the repository:
```
# ansible-playbook --tags <tags> playbook.yml
```

## Current content
* Addition of additional software repositories
* Installation of KDE Plasma
* Installation of various packages
* Profile settings for java-jre
* Automated build of several KDE customization items
* Automated build of vim 8.2
* System wide installation of anaconda3
* Installation of Lmod 

## TODO
* Intel PSXE
* bumblebee
* Auto desktop configuration
